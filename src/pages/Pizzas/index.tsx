import { useFood } from '../../hooks/useFood'
import { Head } from '../../components/Head'
import { Foods } from '../../components/Foods'

export default function Pizzas() {
  const { pizzas } = useFood()

  return (
    <>
      <Head title='Pizzas' description='Itttttttts Pizza Time' />
      <Foods foods={pizzas}></Foods>
    </>
  )
}