import { useFood } from '../../hooks/useFood'

import { Head } from '../../components/Head'
import { Foods } from '../../components/Foods'
import { useTitle } from '../../hooks/useTitle'

export default function Bebidas() {
    const { bebidas } = useFood()
    const { updateTitle } = useTitle()
    updateTitle('Bebidas');

    return (
        <>
            <Head title='Bebidas' />
            <Foods foods={bebidas}></Foods>
        </>
    )
}