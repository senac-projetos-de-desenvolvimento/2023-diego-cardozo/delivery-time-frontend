import { Outlet } from 'react-router-dom'

import { Container } from './styles'

import { NavbarFooter } from '../../components/NavbarFooter'
import { NavbarHeader } from '../../components/NavbarHeader'
import { useTitle } from '../../hooks/useTitle'

export default function Main() {
  const { title } = useTitle()

  return (
    <Container>
      <NavbarHeader />
      <div className="wrapper">
        <h1>{title} </h1>
      </div>

      <section id='main'>
        <div className="wrapper">
          <Outlet />
        </div>
      </section>

      <NavbarFooter />
    </Container>
  )
}