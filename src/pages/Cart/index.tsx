import { Head } from '../../components/Head'
import { Table } from './Table'
import { Container } from './styles'

export default function Main() {
  return (
    <Container>
      <Head title='Carrinho' />
      <Table />
    </Container>
  )
}