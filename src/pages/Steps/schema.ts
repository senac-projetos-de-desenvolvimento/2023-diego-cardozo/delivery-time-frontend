import * as yup from "yup";

export type FieldFormValues = yup.InferType<typeof schema>

export const schema = yup
  .object({
    firstName: yup.string().required("Este campo é obrigatório.!"),
    lastName: yup.string().required("Este campo é obrigatório.!"),
    email: yup
      .string()
      .email()
      .required("O preenchimento de e-mail é obrigatório"),
    mobile: yup.string().required("Você deve informar um telefone"),
  })
  .required();
