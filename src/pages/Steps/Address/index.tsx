import { Head } from '../../../components/Head'

import { useForm, SubmitHandler } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { Container, Form } from '../styles'
import { schema } from '../schema'
import type { FieldFormValues } from '../schema'
import { OrderConfirm } from '../../../components/Order/OrderConfirm'

export default function Address() {
  const {
    // register,
    handleSubmit,
    // formState: { errors },
  } = useForm<FieldFormValues>({
    resolver: yupResolver(schema),
  })
  const onSubmit: SubmitHandler<FieldFormValues> = (data) => console.log(data)

  return (
    <Container>
      <Head title='Endereço' />
      <Form className='mainContent' onSubmit={handleSubmit(onSubmit)}>
        <section>
          <div className='field'>
            <label htmlFor='cep'>CEP</label>
            <input type='text' id='cep' name='cep' />
          </div>

          <div className='field'>
            <label htmlFor='street'>Endereço</label>
            <input type='text' id='street' name='street' />
          </div>

          <div className='grouped'>
            <div className='field'>
              <label htmlFor='number'>Número</label>
              <input type='text' id='number' name='number' />
            </div>

            <div className='field'>
              <label htmlFor='complement'>Complemento</label>
              <input type='text' id='complement' name='complement' />
            </div>
          </div>

          <div className='grouped'>
            <div className='field'>
              <label htmlFor='city'>Cidade</label>
              <input type='text' id='city' value='Pelotas' />
            </div>

            <div className='field'>
              <label htmlFor='state'>Estado</label>
              <select value='RS'>
                <option value=''>Selecione</option>
                <option value='AC'>Acre</option>
                <option value='AL'>Alagoas</option>
                <option value='AP'>Amapá</option>
                <option value='AM'>Amazonas</option>
                <option value='BA'>Bahia</option>
                <option value='CE'>Ceará</option>
                <option value='ES'>Espírito Santo</option>
                <option value='GO'>Goiás</option>
                <option value='MA'>Maranhão</option>
                <option value='MT'>Mato Grosso</option>
                <option value='MS'>Mato Grosso do Sul</option>
                <option value='MG'>Minas Gerais</option>
                <option value='PA'>Pará</option>
                <option value='PB'>Paraíba</option>
                <option value='PR'>Paraná</option>
                <option value='PE'>Pernambuco</option>
                <option value='PI'>Piauí</option>
                <option value='RJ'>Rio de Janeiro</option>
                <option value='RN'>Rio Grande do Norte</option>
                <option value='RS'>Rio Grande do Sul</option>
                <option value='RO'>Rondônia</option>
                <option value='RR'>Roraima</option>
                <option value='SC'>Santa Catarina</option>
                <option value='SP'>São Paulo</option>
                <option value='SE'>Sergipe</option>
                <option value='TO'>Tocantins</option>
                <option value='DF'>Distrito Federal</option>
              </select>
            </div>
          </div>

          <div className='field'>
            <label htmlFor='neighborhood'>Bairro</label>
            <input type='text' id='neighborhood' name='neighborhood' />
          </div>
        </section>

        <OrderConfirm />
      </Form>
    </Container>
  )
}