import { Head } from '../../../components/Head'
import { OrderPay } from '../../../components/Order/OrderPay'

import { useForm, SubmitHandler } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { Container, Form } from '../styles'
import { schema } from '../schema'
import type { FieldFormValues } from '../schema'

export default function Payment() {
  const {
    // register ,
    handleSubmit,
    // formState: { errors },
  } = useForm<FieldFormValues>({
    resolver: yupResolver(schema),
  })
  const onSubmit: SubmitHandler<FieldFormValues> = (data) => console.log(data)

  return (
    <Container>
      <Head title='Pagamento' />
      <Form className='mainContent' onSubmit={handleSubmit(onSubmit)}>
        <section>
          <div className='field'>
            <label htmlFor='credit-card-number'>Número do cartão</label>
            <input
              type='text'
              id='credit-card-number'
              name='credit-card-number'
              autoComplete='cc-number'
            />
          </div>

          <div className='field'>
            <label htmlFor='credit-card-holder-name'>Nome impresso</label>
            <input
              type='text'
              id='credit-card-holder-name'
              name='credit-card-holder-name'
              autoComplete='cc-name'
            />
          </div>

          <div className='grouped'>
            <div className='field'>
              <label htmlFor='credit-card-expiration'>Validade (MM/AA)</label>
              <input
                type='text'
                id='credit-card-expiration'
                name='credit-card-expiration'
                autoComplete='cc-exp'
              />
            </div>

            <div className='field'>
              <label htmlFor='credit-card-code'>CVV</label>
              <input
                type='text'
                id='credit-card-code'
                name='credit-card-code'
                autoComplete='cc-csc'
              />
            </div>
          </div>
        </section>

        <OrderPay />
      </Form>
    </Container>
  )
}