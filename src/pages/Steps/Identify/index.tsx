import { Head } from '../../../components/Head'
import { useForm, SubmitHandler } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { Container, Form } from '../styles'
import { schema } from '../schema'
import type { FieldFormValues } from '../schema'
import { OrderAddress } from '../../../components/Order/OrderAddress'

export default function Identify() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FieldFormValues>({
    resolver: yupResolver(schema),
  })
  const onSubmit: SubmitHandler<FieldFormValues> = (data) => console.log(data)

  return (
    <Container>
      <Head title='Identificação' />
      <Form className='mainContent' onSubmit={handleSubmit(onSubmit)}>
        <section>
          <div className='field'>
            <label htmlFor='firstName'>Nome</label>
            <input type='text' id='firstName' autoComplete='name' {...register('firstName')} />
            {errors.firstName && <p className='error'>{errors.firstName.message}</p>}
          </div>

          <div className='field'>
            <label htmlFor='lastName'>Sobrenome</label>
            <input type='text' id='lastName' autoComplete='name' {...register('lastName')} />
            {errors.lastName && <p className='error'>{errors.lastName.message}</p>}
          </div>

          <div className='field'>
            <label htmlFor='email'>E-mail</label>
            <input type='email' id='email' autoComplete='email' {...register('email')} />
            {errors.email && <p className='error'>{errors.email.message}</p>}
          </div>

          <div className='field'>
            <label htmlFor='document'>CPF</label>
            <input type='text' id='document' name='document' />
          </div>

          <div className='field'>
            <label htmlFor='mobile'>Celular</label>
            <input type='tel' id='mobile' autoComplete='phone' {...register('mobile')} />
            {errors.mobile && <p className='error'>{errors.mobile.message}</p>}
          </div>
        </section>

        <OrderAddress />
      </Form>
    </Container>
  )
}