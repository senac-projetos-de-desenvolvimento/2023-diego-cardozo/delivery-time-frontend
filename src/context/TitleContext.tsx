import { ReactNode, createContext, useState } from 'react'

// import { TitleData } from '../interfaces/TitleData'

interface TitleContextProps {
  title: string;
  updateTitle: (title: string) => void;
}

interface TitleProviderProps {
  children: ReactNode;
}

export const TitleContext = createContext({} as TitleContextProps)

export function TitleProvider({ children }: TitleProviderProps) {
  const [title, setTitle] = useState('Titulo padrão');

  function updateTitle(title: string) {
    setTitle(title);
    return
  }

  return (
    <TitleContext.Provider value={{ title, updateTitle }}>
      {children}
    </TitleContext.Provider>
  )
}

