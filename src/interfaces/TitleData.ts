export interface TitleData {
  title: string
  updateTitle: () => void
}