import { useTitle } from "../../hooks/useTitle";

interface HeadProps {
  title: string
  description?: string
}

export function Head({ title, description = '' }: HeadProps) {
  const { updateTitle } = useTitle()
  updateTitle(title);
  
  document.title = `Delivery Time | ${title}`
  document.querySelector('[name=description]')?.setAttribute('content', description)

  return null
}