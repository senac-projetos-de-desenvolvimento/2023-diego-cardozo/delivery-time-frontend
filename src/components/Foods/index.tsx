import { currencyFormats } from '../../helpers/currencyFormats';
import { FoodData } from '../../interfaces/FoodData';
import { useCart } from '../../hooks/useCart';
import { SkeletonFood } from './SkeletonFood';
import { ReactComponent as IconPlus } from '../../assets/add.svg';

import { Container } from './styles';

interface FoodsProps {
  foods: FoodData[]
}

export function Foods({ foods }: FoodsProps) {
  const { cart, addFoodIntoCart } = useCart()

  if (!foods.length) {
    return (
      <Container>
        {[1, 2].map((n) => <SkeletonFood key={n} />)}
      </Container>
    )
  }

  return (
    <Container>
      {foods.map((food) => {
        const foodExistent = cart.find(
          (item) => item.food === food.food && item.id === food.id,
        )
        return (
          <div key={food.id} className='food'>
            {foodExistent && <span>{foodExistent.quantity}</span>}

            <img src={food.image} alt={food.name} />

            <div className="food__intel">
              <h2>{food.name}</h2>

              <p>{food.description}</p>

              <strong>{currencyFormats(food.price)}</strong>
            </div>

            <button type='button' onClick={() => addFoodIntoCart(food)}>
                <IconPlus />
              </button>
          </div>
        )
      })}
    </Container>
  )
}