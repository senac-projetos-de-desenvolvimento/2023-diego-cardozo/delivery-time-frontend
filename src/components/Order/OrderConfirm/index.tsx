import { useCart } from '../../../hooks/useCart'
import { Container } from '../style'
import { OrderTotal } from '../OrderTotal'

export function OrderConfirm() {
  const { orderConfirm } = useCart()

  return (
    <Container>
      <div className="wrapper">
        <OrderTotal />

        <button type='button' onClick={orderConfirm}>Continuar</button>
      </div>
    </Container>
  )
}