import { Container } from '../style';
import { OrderTotal } from '../OrderTotal';

export function OrderPay() {
  return (
    <Container>
      <div className="wrapper">
        <OrderTotal />

        <button type='submit'>Finalizar Pedido</button>
      </div>
    </Container>
  );
}