import { useCart } from '../../../hooks/useCart'
import { Container } from '../style'
import { OrderTotal } from '../OrderTotal'

export function OrderAddress() {
  const { orderAddress } = useCart()

  return (
    <Container>
      <div className="wrapper">
        <OrderTotal />

        <button type='button' onClick={orderAddress}>Continuar</button>
      </div>
    </Container>
  )
}