import { useCart } from '../../../hooks/useCart'
import { Container } from '../style'
import { OrderTotal } from '../OrderTotal'

export function OrderIdentify() {
  const { orderIdentify } = useCart()

  return (
    <Container>
      <div className="wrapper">
        <OrderTotal />

        <button type='button' onClick={orderIdentify}>Continuar</button>
      </div>
    </Container>
  )
}