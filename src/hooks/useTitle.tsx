import { useContext } from "react";

import { TitleContext } from "../context/TitleContext";

export function useTitle() {
  return useContext(TitleContext)
}